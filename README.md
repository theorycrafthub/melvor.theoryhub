# Melvor Theorycraft Hub

Melvor.TheoryHub is an ASP.NET Core MVC (.NET 6) back-end application with a {not yet defined} Framework front-end.

## Infrastructure
The first step will be to dig deeper in terraform. I personally like to try new technologies (or those I didn't know until now) as long as I can get any use of them. So by digging deeper I mean to develop a proof of concept with a prototype application / application stack to analyze the technology and write a PoC documentation.

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)
